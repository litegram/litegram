# Database migrations

These are the migrations for databases Litegram requires.

These migrations were built for the [Phinx](https://phinx.org/) migrator.

These migrations include:

- The apikeys table (used to store API keys for other services)
- The cache table (used to store temporary information)
- The chat-settings table (used to store specific chat settings)

## How to use these migrations:

1. Install the Phinx migrator
   ```bash
   composer require robmorgan/phinx
   ```

2. Initialize Phinx
   ```bash
   ./vendor/bin/phinx init
   $EDITOR phinx.yml # Check Phinx's documentation for more info
   ```
   It's advised to add phinx.yml to your `.gitignore`

3. Copy this directory to the root of your project
   ```bash
   cp vendor/litegram/framework/db ./
   ```

4. Migrate the database
   ```bash
   ./vendor/bin/phinx migrate
   ```

These migrations as well as Phinx itself are placed automatically when you use
the Litegram skeleton, so in that case skip steps 1 and 3.