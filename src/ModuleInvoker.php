<?php

namespace Litegram;

use Invoker\InvokerInterface;

use Litegram\Telegram\Command\Command;
use Litegram\Telegram\Result\Message;

class ModuleInvoker{

    /** @var InvokerInterface */
    private $invoker;

    public function __construct(InvokerInterface $invoker){
        $this->invoker = $invoker;
    }

    public function call(
    callable $callable, Message $message, array $arguments = []){
        $parameters = [
            'm' => $message
        ];

        $parameters += $arguments;

        return $this->invoker->call($callable, $parameters);
    }
}