<?php

namespace Litegram\Cli\Commands;

use Litegram\Cli\Command;
use Silly\Application;
use Symfony\Component\Console\Output\OutputInterface;
use Litegram\Database\Models\Apikeys;

class Keychain implements Command{

    public function addCommands(Application $app){
        $app->command('apikeys:new name key', [$this, 'add']);
        $app->command('apikeys:rm name', [$this, 'rm']);
        $app->command('apikeys:list', [$this, 'list']);
    }

    public function add(OutputInterface $out, $name, $key){
        $keys = new Apikeys();
        $keys->name = $name;
        $keys->key  = $key;
        $keys->save();
    }

    public function rm(OutputInterface $out, $name){
        $key = Apikeys::where('name', $name);
        $key->delete();
    }

    public function list(OutputInterface $out){
        $keys = Apikeys::all();

        foreach($keys as $key){
            $out->writeln($key->name.'='.$key->key);
        }
    }
}