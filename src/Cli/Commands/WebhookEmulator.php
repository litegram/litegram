<?php

namespace Litegram\Cli\Commands;

use Symfony\Component\Console\Output\OutputInterface;
use Psr\Http\Message\ResponseInterface;
use Litegram\Telegram\Result\Update;
use Litegram\Telegram\Result\Error;
use Litegram\Telegram\Router;
use Litegram\Telegram\Query;
use Litegram\Telegram\Api;
use Litegram\Cli\Command;
use Silly\Application;
use GuzzleHttp\Client;


class WebhookEmulator implements Command{

    /** @var Api */
    private $api;

    /** @var Client */
    private $client;

    /** @var string */
    private $uri;

    public function __construct(Api $api){
        $this->api = $api;

        $this->client = new Client([
            'http_errors' => false
        ]);
    }

    public function addCommands(Application $app){
        $app->command('webhook:emu uri', [$this, 'exec']);
    }

    public function send(Update $u): ResponseInterface{
        return $this->client->request(
            'POST',
            $this->uri,
            [
                'body' => (string) $u
            ]
        );
    }

    public function parseResponse(ResponseInterface $r){
        if($r->getHeader('Content-Type')[0] == 'application/json'){
            $json = json_decode($r->getBody(), true);

            $method = $json['method'] ?? 'intentionalError';
            unset($json['method']);

            $this->api->raw(new Query($method, $json));
            return true;
        }
        return false;
    }

    public function exec(string $uri, OutputInterface $out){
        $this->uri = $uri;

        // Delete the webhook, just in case.
        $this->api->deleteWebhook();

        $uaQuery = new Query('getUpdates');
        $offset  = 0;

        while($ua = $this->api->query($uaQuery)){
            if($ua instanceof Error)
                continue;
            
            foreach($ua as $u){
                $offset = $u->getId()+1;
                $out->write('>> Sending update...');
                $rp = $this->send($u);
                $out->writeln(sprintf(" [%d]",  $rp->getStatusCode()));
                $this->parseResponse($rp);
            }
            
            $uaQuery->setParameter('offset', $offset);
        }
    }
}