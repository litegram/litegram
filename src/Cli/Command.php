<?php

namespace Litegram\Cli;

use Silly\Application;


interface Command{

    public function addCommands(Application $app);

}