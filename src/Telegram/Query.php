<?php

namespace Litegram\Telegram;

use TypeError;
use Psr\Http\Message\ResponseInterface;
use Litegram\Except\TelegramException;

class Query{

    /** @var string */
    private $method;

    /** @var array */
    private $parameters;

    /** @var bool */
    private $hasFiles = false;

    public function __construct(string $method, array $parameters = []){
        $this->method = $method;
        foreach($parameters as $name => $parameter)
            $this->setParameter($name, $parameter);
    }

    public function setParameter(string $name, $value){
        switch($type = gettype($value)){
            case 'array':
                $value = json_encode($value);
            break;
            case 'object':
                if(!($value instanceof InFile))
                    throw new TypeError(
                    'Invalid argument passed to'.__CLASS__.__METHOD__
                    );
                $this->hasFiles = true;
            break;
            case 'string':
            case 'integer':
            case 'double':
            case 'boolean':
                // do nothing
            break;

            default:
                throw new TypeError(
                'Invalid argument passed to'.__CLASS__.__METHOD__
                );
            break;
        }

        $this->parameters[$name] = $value;
        return $this;
    }

    public function setMethod(string $method){
        $this->method = $method;
        return $this;
    }

    public function getMethod(): string{
        return $this->method;
    }

    public function getParameters(bool $forGuzzle=false): array{
        if(!$forGuzzle)
            return $this->parameters;
        
        if(empty($this->parameters))
            return [];
        
        $guzzle = [];
        foreach($this->parameters as $name => $value){
            if($value instanceof InFile)
                $guzzle[$name] = $value->toGuzzle($name);
            else
                $guzzle[$name] = [
                    'name'     => $name,
                    'contents' => $value
                ];
        }

        return ['multipart' => $guzzle];
    }

    public function hasFiles(): bool{
        return $this->hasFiles;
    }

    /**
     * Makes and returns a ResponseInterface to be used in response to webhook
     * POST requests.
     */
    public function getPostResponse(ResponseInterface $res){
        if($this->hasFiles)
            throw new TelegramException(
                "Cannot build response: query contains files");

        $res = $res->withHeader('Content-Type', 'application/json');
        $res->getBody()->write(
            json_encode($this->parameters + ['method' => $this->method]));
        return $res;
    }
}