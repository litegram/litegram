<?php

namespace Litegram\Telegram;

use Psr\Http\Message\StreamInterface;
use TypeError;

/**
 * Files being sent to Telegram.
 */
class InFile{

    const STREAM = 1;

    const CHARS = 2;

    /** @var int */
    private $type;

    /** @var StreamInterface|null */
    private $stream;
    /** @var string|null */
    private $chars;

    /** @var string */
    private $name;

    /**
     * @param StreamInterface|string|resource $file File to send. Can be a file
     * stream, StreamInterface or URL, Telegram file ID or the contents of the
     * file as a string.
     */
    public function __construct($file){
        if($file instanceof StreamInterface || is_resource($file)){
            $this->type = self::STREAM;
            $this->stream = $file;
        }
        elseif(is_string($file)){
            $this->type = self::CHARS;
            $this->string = $file;
        }
        else
            throw new TypeError('Argument passed to '.__CLASS__.' constructor'.
                ' must be either a '.StreamInterface::class.' instance or'.
                ' string. '.get_type($file).' given');
    }

    public function getType(): int{
        return $this->type;
    }

    public function getStream(){
        return $this->stream;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    /**
     * Sets the file name to give Telegram.
     * *Note*: Cannot contain forward slashes (/) or be illegal filenames in any
     * Telegram-supported platform (namely Windows)
     */
    public function setName(string $newname){
        if(preg_match('/\//', $newname))
            throw new TelegramException('File name must not contain /');
        $this->name = $newname;
    }

    /**
     * Converts this InFile to something Guzzle can use.
     * 
     * @param string $parameterName The name of the parameter.
     */
    public function toGuzzle(string $parameterName){
        $contents = ($this->type == self::STREAM) ? 
            $this->stream : $this->chars;

        $gparams = [
            'name'     => $parameterName,
            'contents' => $contents
        ];

        if($this->name !== null) $gparams['filename'] = $this->name;

        return $gparams;
    }
}