<?php

namespace Litegram\Telegram\Modules;

use Litegram\Helpers\Markdown;
use Litegram\Telegram\Result\Message;
use Litegram\Telegram\Result\Chat;
use Litegram\Telegram\Result\User;
use Litegram\Telegram\ChatSettings;
use Litegram\Telegram\Module;
use Litegram\Telegram\Router;
use Litegram\Telegram\Query;
use Litegram\Telegram\Api;
use Litegram\Manual;
use Psr\Container\ContainerInterface;
use Litegram\Except\Id10tException;

/**
 * Contains essential Telegram commands.
 * 
 * **Commands**
 * - /start
 * - /help
 * - /man
 * - /nsfw
 */
class Core implements Module{

    /** @var string */
    const NO_MOTD = <<<TEXT
Help: /help
TEXT;

    /** @var Manual */
    private $man;

    /** @var ChatSettings */
    private $chatSettings;

    /** @var Router */
    private $r;

    /** @var Api */
    private $api;

    /** @var string[] */
    private static $protectedDisables = [
        '/help', '/start', '/disable', '/enable'
    ];

    public function __construct(Manual $m, Api $api, ChatSettings $chatSettings){
        $this->man          = $m;
        $this->chatSettings = $chatSettings;
        $this->api          = $api;
    }

    public function needAdmin(Chat $chat, User $user){
        if($chat->getType() == 'private')
            return;
        if($chat->areAllMembersAdministrators())
            return;

        $member = $this->api->getChatMember([
            'chat_id' => $chat->getId(),
            'user_id' => $user->getId()
        ]);

        switch($member->getStatus()){
            case 'creator':
            case 'administrator':
                return;
                break;
            default:
                throw new Id10tException('You must be an administrator to use this command.');
                break;
        }
    }

    public function addRoutes(Router $r){
        $this->r = $r;
        $r->addCommand('start', [$this, 'start']);
        $r->addCommand('help [command]', [$this, 'help']);
        $r->addCommand('nsfw [option]', [$this, 'nsfw']);
        $r->addCommand('man page', [$this, 'man']);
    }

    public function start(Message $m){
        $motd = $this->man->getPage('misc.motd') ?? self::NO_MOTD;
        return $m->reply($motd);
    }

    public function help(Message $m, $command = null){
        $q = $m->reply('');

        if($command === null){
            $list = Markdown::bold('Available commands:').PHP_EOL;
            foreach($this->r->getCommands() as $name => $c){
                $list.= Markdown::escape('/'.$c->getExpression()."\n");
            }
            return Markdown::setMode($q)->setParameter('text', $list);
        }
        else{
            if($man = $this->man->getCommandHelp($command)){
                $man = str_replace('{{name}}', $command, $man);
                return $q->setParameter('text', $man)
                    ->setParameter('parse_mode', 'Markdown');
            }else
                return $q->setParameter('text', 'Command not found');
        }
    }

    public function nsfw(Message $m, string $option = null){
        $c = $m->getChat();
        if($c->getType() == 'private')
            return $m->reply('This command only works in groups');

        if($option){
            $this->needAdmin($m->getChat(), $m->getFrom());
            $bopt = filter_var($option, FILTER_VALIDATE_BOOLEAN);
            $this->chatSettings->setNsfw($c->getId(), $bopt);
            $out = 'NSFW features have been turned '.($bopt ? 'on' : 'off');
        }else{
            $bopt = $this->chatSettings->isNsfw($c->getId());
            $out  = 'NSFW features are '.($bopt ? 'on' : 'off');
        }
        return $m->reply($out);
    }

    public function man(Message $m, string $page){
        if($man = $this->man->getPage($page)){
            return $m->reply($man)
                ->setParameter('parse_mode', 'Markdown');
        }else{
            return $m->reply('No manual page for '.$page);
        }
    }
}