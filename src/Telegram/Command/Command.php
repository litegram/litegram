<?php

namespace Litegram\Telegram\Command;

class Command{

    /** @var InputArgument[] */
    private $arguments;

    /** @var callable */
    private $handler;

    /** @var string */
    private $name;

    public function __construct(string $name, array $args, callable $handler){
        $this->name      = $name;
        $this->arguments = $args;
        $this->handler   = $handler;
    }

    public function getName(): string{
        return $this->name;
    }

    public function getArguments(): array{
        return $this->arguments;
    }

    public function getHandler(): callable{
        return $this->handler;
    }

    public function isValid(string $expr): bool{
        $tokens = $this->unpackExpression($expr);
        $name   = trim(array_shift($tokens), '/');

        // Strip username from command name
        $name = preg_replace('/@.+$/', '', $name);

        if($name != $this->name)
            return false;
        
        foreach($this->arguments as $n => $arg){
            $token = $tokens[$n] ?? null;

            if($token == null && !$arg->isOptional())
                return false;
        }

        return true;
    }

    public function getArgList(string $expr){
        if(!$this->isValid($expr))
            return null;
        
        $tokens = $this->unpackExpression($expr);
        array_shift($tokens);

        $list = [];
        foreach($tokens as $n => $token){
            $arg = $this->arguments[$n] ?? null;

            if($arg == null)
                break;

            if($arg->isVariableLength()){
                $list[$arg->getName()] = $tokens;
                return $list;
            }
            $list[$arg->getName()] = $token;
            array_shift($tokens);
        }

        return $list;
    }

    public function unpackExpression(string $expr){
        $tokens = explode(' ', $expr);
        $tokens = array_map('rtrim', $tokens);
        $tokens = array_values(array_filter($tokens));
        return $tokens;
    }

    public function getExpression(): string{
        $expr = $this->name;
        foreach($this->arguments as $arg){
            if($arg->isOptional())
                $expr.= ' ['.$arg->getName().']';
            else
                $expr.= ' '.$arg->getName();
            if($arg->isVariableLength())
                $expr.= '*';
        }

        return $expr;
    }
}