<?php

namespace Litegram\Telegram\Command;

use Litegram\Except\LitegramException;


/**
 * Parse a given command expression.
 * 
 * Derivative of:
 * https://github.com/mnapoli/silly/blob/master/src/Command/ExpressionParser.php
 */
class ExpressionParser{

    public function parse(string $expr){
        $tokens = explode(' ', $expr);
        $tokens = array_map('rtrim', $tokens);
        $tokens = array_values(array_filter($tokens));

        if(empty($tokens))
            throw new LitegramException('Empty expression');
        
        $name = array_shift($tokens);

        $arguments = [];
        foreach($tokens as $n => $token){
            $arg = $this->parseArgument($token);

            if($arg->isVariableLength() && $n != count($tokens) -1)
                throw
                new LitegramException('Variable-length arguments must be last');

            $arguments[] = $this->parseArgument($token);
        }
        
        return [
            'name' => $name,
            'arguments' => $arguments
        ];
    }

    public function parseArgument(string $token): InputArgument{
        if ($this->endsWith($token, ']*')){
            $mode = InputArgument::VARIABLE;
            $name = trim($token, '[]*');
        }
        elseif ($this->endsWith($token, '*')){
            $mode = InputArgument::VARIABLE | InputArgument::REQUIRED;
            $name = trim($token, '*');
        }
        elseif ($this->startsWith($token, '[')){
            $mode = InputArgument::OPTIONAL;
            $name = trim($token, '[]');
        }
        else {
            $mode = InputArgument::REQUIRED;
            $name = $token;
        }

        return new InputArgument($name, $mode);
    }

    private function startsWith($haystack, $needle){
        return substr($haystack, 0, strlen($needle)) === $needle;
    }
    private function endsWith($haystack, $needle){
        return substr($haystack, -strlen($needle)) === $needle;
    }
}