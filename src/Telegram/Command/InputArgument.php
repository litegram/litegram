<?php

namespace Litegram\Telegram\Command;

class InputArgument{

    /** Optional argument */
    const OPTIONAL = 0;
    /** Required argument  */
    const REQUIRED = 1;
    /** Variable-length list */
    const VARIABLE = 2;

    /** @var string */
    private $name;

    /** @var int */
    private $mode;

    public function __construct(string $name, int $mode){
        $this->name = $name;
        $this->mode = $mode;
    }

    public function getName(): string{
        return $this->name;
    }

    public function isOptional(): bool{
        return !($this->mode & self::REQUIRED);
    }

    public function isVariableLength(): bool{
        return ($this->mode & self::VARIABLE);
    }
}