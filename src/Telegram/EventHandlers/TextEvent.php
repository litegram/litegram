<?php

namespace Litegram\Telegram\EventHandlers;

class TextEvent{

    /** @var string */
    private $regex;

    /** @var callable */
    private $handler;

    public function __construct(string $regex, callable $handler){
        $this->regex   = $regex;
        $this->handler = $handler;
    }

    public function check(string $text): bool{
        return preg_match($this->regex, $text);
    }
}