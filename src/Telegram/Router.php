<?php

namespace Litegram\Telegram;

use Litegram\Telegram\EventHandlers\TextEvent;
use Litegram\Telegram\Command\ExpressionParser;
use Litegram\Telegram\Command\Command;
use Litegram\Telegram\Result\Message;
use Litegram\Telegram\Query;
use Litegram\Except\Id10tException;
use Litegram\ModuleInvoker;
use DI\Container;

class Router{

    /** @var array */
    private $commands = [];

    /** @var array[] */
    private $textEvents = [];

    /** @var ExpressionParser */
    private $expressionParser;

    /** @var ModuleInvoker */
    private $moduleInvoker;

    /** @var Api */
    private $api;

    /** @var ChatSettings */
    private $chatSettings;

    public function __construct(Container $c){  
        $this->api = $c->get(Api::class);
        $this->expressionParser = new ExpressionParser();

        $modules = $c->get('telegram.modules');
        foreach($modules as $module){
            $m = $c->get($module);
            $m->addRoutes($this);
        }
        $this->moduleInvoker = $c->get(ModuleInvoker::class);
        $this->chatSettings  = $c->get(ChatSettings::class);
    }

    /**
     * Adds a command to the router.
     * 
     * Command expression consists of the name and any arguments.
     * 
     * `foo bar [baz] [far]*`
     * 
     * In this example, foo is the command, bar is a required argument, baz is
     * an optional argument and far is an optional variable-length argument.
     * 
     * The command is handler is called by the PHP-DI Invoker and provides the
     * Message object via $m as well as the arguments via their given names in
     * $expr.
     * 
     * @param string      $expr Command expression
     * @param callable $handler Command handler
     */
    public function addCommand(string $expr, callable $handler){
        list('name' => $name, 'arguments' => $args) = 
            $this->expressionParser->parse($expr);
        $this->commands[$name] = new Command($name, $args, $handler);
    }

    public function addTextEvent(string $name, string $regex, callable $handlr){
        $this->textEvents[$regex][$name] = $handlr;
    }

    /**
     * @param Message $m
     * 
     * @return Query|void
     */
    public function dispatch(Message $m){
        if($m->getType() != Message::TEXT) return;

        if($m->isCommand()){
            $cName = $m->getCommand(true);
            $c = $this->commands[$cName] ?? null;

            if($c){
                return $this->handleCommand($c, $m);
            }
        }else{
            $this->handleTextEvents($m);
        }
    }

    private function handleTextEvents(Message $m){
        foreach($this->textEvents as $regex => $event){
            if(preg_match($regex, $m->getText())){
                foreach($this->textEvents[$regex] as $name => $handler){
                        $out = $this->moduleInvoker->call($handler, $m);
                    if($out instanceof Query)
                        $this->api->raw($out);
                }
            }
        }
    }

    private function handleCommand(Command $c, Message $m){
        $expr = $m->getText();
        // Check args
        if(!$c->isValid($expr)){
            return new Query('sendMessage', [
                'chat_id' => $m->getChat()->getId(),
                'text'    => sprintf('Usage: /%s', $c->getExpression())
            ]);
        }

        try{
            $handler = $c->getHandler();
            $arglist = $c->getArgList($expr);
            $arglist['m'] = $m;
            return $this->moduleInvoker->call($handler, $m, $arglist);
        }catch(Id10tException $e){
            return new Query('sendMessage', [
                'chat_id' => $m->getChat()->getId(),
                'text'    => $e->getMessage()
            ]);
        }
    }

    /**
     * Lists all commands currently registered with the router.
     */
    public function listCommands(): array{
        return array_keys($this->commands);
    }

    public function getCommands(): array{
        return $this->commands;
    }

    public function checkArgc(string $check, Message $m): bool{
        $argc = count($m->getWords()) - 1;

        if(empty($check)) return true;

        if(is_numeric($check)) return $argc == $check;

        list($min, $max) = explode(';', $check);

        if(empty($min)) $min = null;
        if(empty($max)) $max = null;

        return ($min ?? 0 <= $argc && $argc <= $max ?? INF);
    }
}