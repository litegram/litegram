<?php

namespace Litegram\Telegram;

interface Module{

    /**
     * Add commands and events to the Telegram router.
     * 
     * @param Manager $tgm
     */
    public function addRoutes(Router $r);
}