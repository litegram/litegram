<?php

namespace Litegram\Telegram;

use Litegram\Except\TelegramException;

use Litegram\Telegram\Result\Result;
use Litegram\Telegram\Result\Error;
use Litegram\Telegram\Result\Message;
use Litegram\Telegram\Result\SimpleResult;
use Litegram\Telegram\Result\Chat;
use Litegram\Telegram\Result\User;
use Litegram\Telegram\Result\File;
use Litegram\Telegram\Result\WebhookInfo;
use Litegram\Telegram\Result\UpdateArray;
use Litegram\Telegram\Result\ChatMember;
use Litegram\Telegram\Result\UserProfilePhotos;
use Litegram\Telegram\Result\Update;

class Parser{

    /**
     * Parses raw Telegram output as PHP objects.
     * 
     * @param string $raw API output
     * @param string $method Method used
     * 
     * @throws TelegramException
     * @return Result
     */
    public static function parse(string $raw, string $method): Result{
        $json = json_decode($raw);

        if($json === null)
            throw new TelegramException('Could not parse: API output corrupt');

        if(!$json->ok)
            return new Error($json);

        switch(strtolower($method)){
            // Message-sending methods
            case 'sendmessage':
			case 'sendphoto':
			case 'sendaudio':
			case 'senddocument':
			case 'sendsticker':
			case 'sendvideo':
			case 'sendvoice':
			case 'sendlocation':
            case 'forwardmessage':
                return new Message($json->result);
            break;

            case 'getchat':
                return new Chat($json->result);

            case 'getme':
                return new User($json->result);
            break;

            case 'getfile':
				return new File($json->result);
            break;
            
            case 'getwebhookinfo':
                return new WebhookInfo($json->result);
            break;

            case 'getupdates':
                return new UpdateArray($json->result);
            break;

            case 'getchatmember':
                return new ChatMember($json->result);
            break;

            case 'getuserprofilephotos':
                return new UserProfilePhotos($json->result);
            break;

            // Methods that return single values like an int or bool.
            case 'setwebhook':
			case 'deletewebhook':
			case 'sendchataction':
			case 'kickchatmember':
			case 'unbanchatmember':
			case 'leavechat':
			case 'answercallbackquery':
            case 'getchatmemberscount':
            // Catch-all
            default:
                return new Result($json->result);
            break;
        }
    }

    public static function parseWebhook(string $body){
        $json = json_decode($body);
        return new Update($json);
    }
}