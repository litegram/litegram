<?php

namespace Litegram\Telegram;

use Litegram\Database\Models\ChatSetting;

class ChatSettings{

    private static $defaults = [
        'nsfw'     => false
    ];

    public function getAllSettings(int $chatId){
        $query = ChatSetting::where('chat_id', $chatId);

        if($query->count() == 0){
            $this->setDefaults($chatId);
            return self::$defaults;
        }
        
        $settings = $query->first();

        return [
            'nsfw'     => (bool) $settings->nsfw
        ];
    }

    public function setDefaults(int $chatId){
        $settings = new ChatSetting();

        $settings->chat_id  = $chatId;
        $settings->nsfw     = self::$defaults['nsfw'];
        $settings->save();
    }

    public function isNsfw(int $chatId): bool{
        return $this->getAllSettings($chatId)['nsfw'];
    }

    public function setNsfw(int $chatId, bool $newSetting){
        // Set defaults in case settings for this chat don't exist
        $this->getAllSettings($chatId);

        ChatSetting::where('chat_id', $chatId)
            ->update(['nsfw' => $newSetting]);
    }

}