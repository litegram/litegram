<?php

namespace Litegram\Telegram;

use Litegram\Telegram\Result\File;
use Psr\Http\Message\StreamInterface;

/**
 * File that came from Telegram's servers.
 */
class OutFile{

    /** @var StreamInterface */
    private $stream;

    /** @var File */
    private $file;

    public function __construct(File $file, StreamInterface $stream){
        $this->file   = $file;
        $this->stream = $stream;
    }

    public function getStream(): StreamInterface{
        return $this->stream;
    }
}