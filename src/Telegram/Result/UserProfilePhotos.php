<?php

namespace Litegram\Telegram\Result;

class UserProfilePhotos extends ResultArray{

    /** @var int */
    private $totalCount;

    protected function parse(){
        $json = $this->json;

        $this->totalCount = $json->total_count;

        foreach($json->photos as $photo){
            $this->parseResult($photo);
        }
    }

    protected function parseResult($r){
        $this->results[] = new PhotoSizeArray($r);
    }

    public function getTotalCount(): int{
        return $this->totalCount;
    }
}