<?php

namespace Litegram\Telegram\Result;

class File extends Result{

    public function getFileId(): string{
        return $this->json->file_id;
    }

    /** @return int|null */
    public function getFileSize(){
        return $this->json->file_size ?? null;
    }

    /** @return string|null */
    public function getFilePath(){
        return $this->json->file_path ?? null;
    }
}