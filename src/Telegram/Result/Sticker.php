<?php

namespace Litegram\Telegram\Result;

class Sticker extends Result{

    /** @var PhotoSize|null */
    protected $thumb;
    
    /** @var MaskPosition|null */
    protected $maskPosition;

	public function parse(){
		$json = $this->json;

		if(isset($json->thumb))
            $this->thumb = new PhotoSize($json->thumb);
        
        if(isset($json->mask_position))
            $this->maskPosition = new MaskPosition($json->mask_position);
    }

    public function getFileId(): string{
		return $this->json->file_id;
    }
    
    public function getWidth(): int{
        return $this->json->width;
    }

    public function getHeight(): int{
        return $this->json->height;
    }

    /** @return PhotoSize|null */
	public function getThumb(){
		return $this->thumb;
    }
    
    /** @return string|null */
    public function getEmoji(){
        return $this->json->emoji ?? null;
    }

    /** @return string|null */
    public function getSetName(){
        return $this->json->set_name ?? null;
    }

    /** @return MaskPosition|null */
    public function getMaskPosition(){
        return $this->maskPosition;
    }

	/** @return int|null */
	public function getFileSize(){
		return $this->json->file_size ?? null;
	}
}