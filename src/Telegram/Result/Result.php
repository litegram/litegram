<?php

namespace Litegram\Telegram\Result;

use JsonSerializable;

/**
 * Parsed Telegram API output.
 */
class Result implements JsonSerializable{

    /** @var mixed */
    protected $json;

    public final function __construct($json){
        $this->json = $json;
        $this->parse();
    }

    /**
     * Parses the JSON output of the API.
     */
    protected function parse(){

    }

    public final function __toString(): string{
        return $this->toJson();
    }

    public function toJson(): string{
        return json_encode($this->getRaw());
    }

    public function jsonSerialize(){
        return $this->getRaw();
    }

    public function getRaw(){
        return $this->json;
    }

    public function isError(){
        return static::class === Error::class;
    }
}