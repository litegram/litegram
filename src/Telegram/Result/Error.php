<?php

namespace Litegram\Telegram\Result;

class Error extends Result{

    /** @var int */
    private $code;

    /** @var string */
    private $description;

    protected function parse(){
        $json = $this->json;

        $this->code = $json->error_code;
        $this->description = $json->description;
    }

    public function getCode(): int{
        return $this->code;
    }

    public function getDescription(): string{
        return $this->description;
    }
}