<?php

namespace Litegram\Telegram\Result;

class MaskPosition extends Result{

    public function getPoint(): string{
        return $this->json->point;
    }

    public function getXShift(): float{
        return $this->json->x_shift;
    }

    public function getYShift(): float{
        return $this->json->y_shift;
    }

    public function getScale(): float{
        return $this->json->scale;
    }
}