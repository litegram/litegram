<?php

namespace Litegram\Telegram\Result;

class Chat extends Result{

    /** @var int */
    private $id;

    /** @var string */
    private $type;

    /** @var string|null */
    private $username;

    /** @var bool|null */
    private $isAllAdmin;

    /** @var string|null */
    private $title;

    /** @var string|null */
    private $firstName;

    /** @var string|null */
    private $lastName;

    protected function parse(){
        $json = $this->json;

        $this->id   = $json->id;
        $this->type = $json->type;

        $this->username   = $json->username ?? null;
        $this->isAllAdmin = $json->all_members_are_administrators ?? null;

        $this->title = $json->title ?? null;
        
        $this->firstName = $json->first_name ?? null;
        $this->lastName  = $json->last_name ?? null;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getType(): string{
        return $this->type;
    }

    public function getUsername(){
        return $this->username;
    }

    public function areAllMembersAdministrators(){
        return $this->isAllAdmin;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getFirstName(){
        return $this->firstName;
    }

    public function getLastName(){
        return $this->lastName;
    }

    public function getFullName(){
        $full = $this->firstName;
        if($this->lastName)
            $full.= ' '.$this->lastName;
        return $full;
    }
}