<?php

namespace Litegram\Telegram\Result;

class User extends Result{

    /** @var int */
    private $id;

    /** @var string|null */
    private $username;

    /** @var string */
    private $firstName;

    /** @var string|null */
    private $lastName;

    public function parse(){
        $json = $this->json;

        $this->id = $json->id;

        $this->username = $json->username ?? null;

        $this->firstName = $json->first_name;
        $this->lastName  = $json->last_name ?? null;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getFirstName(): string{
        return $this->firstName;
    }

    public function getLastName(){
        return $this->lastName;
    }

    public function getFullName(): string{
        $full = $this->firstName;
        if($this->lastName)
            $full.= ' '.$this->lastName;
        return $full;
    }
}