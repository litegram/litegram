<?php

namespace Litegram\Telegram\Result;

class Update extends Result{

    /** Message */
    const MESSAGE = 1;
    /** Edited message */
    const ED_MESSAGE = 2;
    /** Channel post */
    const POST = 4;
    /** Edited channel post */
    const ED_POST = 8;
    /** Inline query */
    const INLINE_QUERY = 0x10;
    /** Chosen inline result */
    const CHOSEN_INLINE = 0x20;
    /** Callback query */
    const CALLBACK_QUERY = 0x40;
    /** Shipping query */
    const SHIPPING_QUERY = 0x80;
    /** Pre-checkout query */
    const PRE_CHECKOUT_QUERY = 0x100;

    /** @var int */
    private $type;

    /** @var int */
    private $id;

    /** @var Message */
    private $message;

    protected function parse(){
        $json = $this->json;

        $this->id = $json->update_id;

        if(isset($json->message)){
            $this->type = self::MESSAGE;
            $this->message = new Message($json->message);
        }

        if(isset($json->edited_message)){
            $this->type = self::ED_MESSAGE;
            $this->message = new Message($json->edited_message);
        }

        if(isset($json->channel_post)){
            $this->type = self::POST;
            $this->message = new Message($json->channel_post);
        }

        if(isset($json->edited_channel_post)){
            $this->type = self::CHANNEL_POST;
            $this->message = new Message($json->edited_channel_post);
        }

        // --- CURRENTLY UNSUPPORTED ---

        if(isset($json->inline_query)){
            $this->type = self::INLINE_QUERY;
        }

        if(isset($json->chosen_inline_result)){
            $this->type = self::CHOSEN_INLINE;
        }

        if(isset($json->callback_query)){
            $this->type = self::CALLBACK_QUERY;
        }

        if(isset($json->shipping_query)){
            $this->type = self::SHIPPING_QUERY;
        }

        if(isset($json->pre_checkout_query)){
            $this->type = self::PRE_CHECKOUT_QUERY;
        }
    }

    public function getId(): int{
        return $this->id;
    }

    public function getType(): int{
        return $this->type;
    }

    public function getMessage(){
        return $this->message;
    }
}