<?php

namespace Litegram\Telegram\Result;

class ChatMember extends Result{

    /** @var User */
    private $user;

    /** @var string */
    private $status;

    protected function parse(){
        $json = $this->json;

        $this->user   = new User($json->user);
        $this->status = $json->status;
    }

    public function getUser(): User{
        return $this->user;
    }

    public function getStatus(): string{
        return $this->status;
    }
}