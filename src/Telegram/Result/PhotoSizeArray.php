<?php

namespace Litegram\Telegram\Result;

class PhotoSizeArray extends ResultArray{

    protected function parseResult($r){
        $this->results[] = new PhotoSize($r);
    }
}