<?php

namespace Litegram\Telegram\Result;

class UpdateArray extends ResultArray{

    protected function parseResult($r){
        $u = new Update($r);
        $this->results[] = $u;
    }

}