<?php

namespace Litegram\Telegram\Result;

class Location extends Result{

    public function getLatitude(): float{
        return $this->json->latitude;
    }

    public function getLongitude(): float{
        return $this->json->longitude;
    }
}