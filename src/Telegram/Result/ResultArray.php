<?php

namespace Litegram\Telegram\Result;

use ArrayAccess;
use Iterator;

abstract class ResultArray extends Result implements ArrayAccess, Iterator{

    /** @var Result[] */
    protected $results = [];

    /** @var int */
    private $pointer;

    // ArrayAccess

    public function offsetExists($offset): bool{
        return isset($this->results[$offset]);
    }

    public function offsetGet($offset){
        return $this->results[$offset];
    }

    public function offsetSet($offset, $value){

    }

    public function offsetUnset($offset){

    }

    // Iterator

    public function current(){
        return $this->results[$this->pointer];
    }

    public function key(){
        return $this->pointer;
    }

    public function rewind(){
        $this->pointer = 0;
    }

    public function next(){
        $this->pointer++;
    }

    public function valid(){
        return isset($this->results[$this->pointer]);
    }

    // Result parsing

    protected abstract function parseResult($result);

    protected function parse(){
        foreach($this->json as $r){
            $this->parseResult($r);
        }
    }
}