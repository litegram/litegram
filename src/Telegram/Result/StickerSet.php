<?php

namespace Litegram\Telegram\Result;

class StickerSet extends ResultArray{

    protected function parse(){
        $json = $this->json;

        foreach($json->stickers as $sticker)
            $this->results[] = new Sticker($sticker);
    }

    public function getName(): string{
        return $this->json->name;
    }

    public function getTitle(): string{
        return $this->json->title;
    }

    public function hasMasks(): bool{
        return $this->json->contains_masks;
    }
}