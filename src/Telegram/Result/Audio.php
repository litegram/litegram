<?php

namespace Litegram\Telegram\Result;

class Audio extends Result{

    /** @var string */
    private $fileId;

    /** @var int */
    private $duration;

    /** @var string */
    private $performer;

    /** @var string */
    private $title;

    /** @var string */
    private $mimeType;

    /** @var int */
    private $fileSize;

    protected function parse(){
        $json = $this->json;

        $this->fileId   = $json->file_id;
        $this->duration = $json->duration;

        $this->performer = $json->performer ?? null;
        $this->title     = $json->title ?? null;
        $this->mimeType  = $json->mime_type ?? null;
        $this->fileSize  = $json->file_size ?? null;
    }

    public function getFileId(): string{
        return $this->fileId;
    }

    public function getDuration(): int{
        return $this->duration;
    }

    public function getPerformer(){
        return $this->performer;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getMimeType(){
        return $this->mimeType;
    }

    public function getFileSize(){
        return $this->fileSize;
    }
}