<?php

namespace Litegram\Telegram\Result;

class Voice extends Result{

    public function getFileId(): string{
        return $this->json->file_id;
    }

    public function getDuration(): int{
        return $this->json->duration;
    }

    /** @return string|null */
    public function getMimeType(){
        return $this->json->mime_type ?? null;
    }

    /** @return int|null */
    public function getFileSize(){
        return $this->json->file_size ?? null;
    }
}