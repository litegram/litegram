<?php

namespace Litegram\Telegram\Result;

use DateTime;

class WebhookInfo extends Result{

    /** @var DateTime|null */
    private $lastErrorDate;

    public function parse(){
        $json = $this->json;

        if(isset($json->last_error_date)){
            $this->lastErrorDate = new DateTime();
            $this->lastErrorDate->setTimestamp($json->last_error_date);
        }
    }

    public function getUrl(): string{
        return $this->json->url;
    }

    public function hasCustomCertificate(): bool{
        return $this->json->has_custom_certificate;
    }

    public function getPendingUpdateCount(): int{
        return $this->json->pending_update_count;
    }

    /** @return DateTime|null */
    public function getLastErrorDate(){
        return $this->lastErrorDate;
    }

    /** @var string|null */
    public function getLastErrorMessage(){
        return $this->json->last_error_message ?? null;
    }

    /** @var int|null */
    public function getMaxConnections(){
        return $this->json->max_connections ?? null;
    }

    /** @var string[]|null */
    public function getAllowedUpdates(){
        return $this->json->allowed_updates ?? null;
    }
}