<?php

namespace Litegram\Telegram\Result;

class PhotoSize extends Result{

    /** @var string */
    private $fileId;

    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var int */
    private $fileSize;

    protected function parse(){
        $json = $this->json;

        $this->fileId = $json->file_id;
        $this->width  = $json->width;
        $this->height = $json->height;

        $this->fileSize = $json->file_size ?? null;
    }

    public function getFileId(): string{
        return $this->fileId;
    }

    public function getWidth(): int{
        return $this->width;
    }

    public function getHeight(): int{
        return $this->height;
    }

    /** @var int|null */
    public function getFileSize(){
        return $this->fileSize;
    }
}