<?php

namespace Litegram\Telegram\Result;

class Document extends Result{

	/** @var PhotoSize|null */
	protected $thumb;

	public function parse(){
		$json = $this->json;

		if(isset($json->thumb))
			$this->thumb = new PhotoSize($json->thumb);
	}

	public function getFileId(): string{
		return $this->json->file_id;
	}

	/** @return PhotoSize|null */
	public function getThumb(){
		return $this->thumb;
	}

	/** @return string|null */
	public function getFileName(){
		return $this->json->file_name ?? null;
	}

	/** @return string|null */
	public function getMimeType(){
		return $this->json->mime_type ?? null;
	}

	/** @return int|null */
	public function getFileSize(){
		return $this->json->file_size ?? null;
	}
}
