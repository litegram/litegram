<?php

namespace Litegram\Telegram\Result;

class Contact extends Result{

    public function getPhoneNumber(): string{
        return $this->json->phone_number;
    }

    public function getFirstName(): string{
        return $this->json->first_name;
    }

    /** @return string|null */
    public function getLastName(){
        return $this->json->last_name ?? null;
    }

    /** @return int|null */
    public function getUserId(){
        return $this->json->user_id ?? null;
    }
}