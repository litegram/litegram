<?php

namespace Litegram\Telegram\Result;

use DateTime;
use Litegram\Telegram\Query;

/**
 * @{inheritdoc}
 */
class Message extends Result{

    /** Text message */
    const TEXT = 1;
    /** Audio */
    const AUDIO = 2;
    /** Document/GIF/uncompressed photo */
    const DOCUMENT = 4;
    /** Photo */
    const PHOTO = 8;
    /** Sticker */
    const STICKER = 0x10;
    /** Video */
    const VIDEO = 0x20;
    /** Voice note */
    const VOICE = 0x40;
    /** Contact */
    const CONTACT = 0x80;
    /** Location */
    const LOCATION = 0x100;
    /** New chat member */
    const JOIN = 0x200;
    /** Leaving chat member */
    const LEFT = 0x400;
    /** Updated chat title */
    const N_TITLE = 0x800;
    /** New chat photo */
    const N_CPHOTO = 0x1000;
    /** Deleted chat photo */
    const D_CPHOTO = 0x2000;
    /** Group created */
    const N_GROUP = 0x4000;
    /** Channel created */
    const N_CHANNEL = 0x8000;
    /** Convert to supergroup */
    const SUPER = 0x10000;
    /** Pinned message */
    const PINNED_MESSAGE = 0x20000;
    /** Unknown type */
    const UNKNOWN = 0x4000000000000000;

    /** @var int */
    private $id;

    /** @var DateTime */
    private $date;

    /** @var Chat */
    private $chat;

    /** @var User|null */
    private $from;

    /** @var Message|null */
    private $replyTo;

    /** @var bool */
    private $isForward = false;
    /** @var User|null */
    private $forwardFrom;
    /** @var DateTime|null */
    private $forwardDate;
    /** @var Chat|null */
    private $forwardFromChat;

    /** @var int */
    private $type = self::UNKNOWN;

    /** @var string|null */
    private $text;
    /** @var string[] */
    private $words = [];
    /** @var bool */
    private $isCommand = false;
    /** @var string|null */
    private $command;

    /** @var Audio|null */
    private $audio;
    /** @var Document|null */
    private $document;
    /** @var PhotoSizeArray|null */
    private $photo;
    /** @var Sticker|null */
    private $sticker;
    /** @var Video|null */
    private $video;
    /** @var Voice|null */
    private $voice;
    /** @var Contact|null */
    private $contact;
    /** @var Location|null */
    private $location;
    
    /** @var User|null */
    private $joined;
    /** @var User|null */
    private $left;
    /** @var string|null */
    private $newTitle;
    /** @var PhotoSizeArray|null */
    private $newPhoto;
    /** @var Message|null */
    private $pin;


    /**
     * @{inheritdoc}
     */
    protected function parse(){
        $json = $this->json;

        $this->id   = $json->message_id;

        $this->date = new DateTime();
        $this->date->setTimestamp($json->date);

        $this->chat = new Chat($json->chat);

        if(isset($json->from))
            $this->from = new User($json->from);

        if(isset($json->forward_from)){
            $this->isForward   = true;
            $this->forwardFrom = new User($json->forward_from);
            $this->forwardDate = new DateTime();
            $this->forwardDate->setTimestamp($json->forward_date);
            if(isset($json->forward_from_chat))
                $this->forwardFromChat = $json->forward_from_chat;
        }

        if(isset($json->reply_to_message))
            $this->replyTo = new static($json->reply_to_message);
        
        //
        // PREPARE FOR ABSOLUTE CHAOS
        //

        if(isset($json->text)){
            $this->type  = self::TEXT;
            $this->text  = $json->text;
            $this->words = preg_split('/\s/', $this->text);
            if(strpos($this->text, '/') === 0){
                $this->isCommand = true;
                $this->command   = substr($this->words[0], 1);
            }
        }
        elseif(isset($json->audio)){
            $this->type  = self::AUDIO;
            $this->audio = new Audio($json->audio);
        }
        elseif(isset($json->document)){
            $this->type = self::DOCUMENT;
            $this->document = new Document($json->document);
        }
        elseif(isset($json->photo)){
            $this->type = self::PHOTO;
            $this->photo = new PhotoSizeArray($json->photo);
        }
        elseif(isset($json->sticker)){
            $this->type = self::STICKER;
            $this->sticker = new Sticker($json->sticker);
        }
        elseif(isset($json->video)){
            $this->type = self::VIDEO;
            $this->video = new Video($json->video);
        }
        elseif(isset($json->voice)){
            $this->type = self::VOICE;
            $this->voice = new Voice($json->voice);
        }
        elseif(isset($json->contact)){
            $this->type = self::CONTACT;
            $this->contact = new Contact($json->contact);
        }
        elseif(isset($json->location)){
            $this->type = self::LOCATION;
            $this->location = new Location($json->location);
        }
        elseif(isset($json->new_chat_participant)){
            $this->type = self::JOIN;
            $this->joined = new User($json->new_chat_participant);
        }
        elseif(isset($json->left_chat_participant)){
            $this->type = self::LEFT;
            $this->left = new User($json->left_chat_participant);
        }
        elseif(isset($json->new_chat_title)){
            $this->type = self::N_TITLE;
            $this->newTitle = $json->new_chat_title;
        }
        elseif(isset($json->new_chat_photo)){
            $this->type = self::N_CPHOTO;
            $this->newPhoto = new PhotoSizeArray($json->new_chat_photo);
        }
        elseif(isset($json->delete_chat_photo)){
            $this->type = self::D_CPHOTO;
        }
        elseif(isset($json->group_chat_created)){
            $this->type = self::N_GROUP;
        }
        elseif(isset($json->channel_chat_created)){
            $this->type = self::N_CHANNEL;
        }
        elseif(isset($json->supergroup_chat_created)){
            $this->type = self::SUPER;
        }
        elseif(isset($json->pinned_message)){
            $this->type = self::PINNED_MESSAGE;
            $this->pin  = new Message($json->pinned_message);
        }
    }

    public function getId(): int{
        return $this->id;
    }

    public function getDate(): DateTime{
        return $this->date;
    }

    public function getChat(): Chat{
        return $this->chat;
    }

    public function getFrom(): User{
        return $this->from;
    }

    /** @return Message|null */
    public function getReplyTo(){
        return $this->replyTo;
    }

    public function isForward(): bool{
        return $this->isForward;
    }

    public function getForwardFrom(){
        return $this->forwardFrom;
    }

    public function getForwardDate(){
        return $this->forwardDate;
    }

    public function getForwardFromChat(){
        return $this->forwardFromChat;
    }

    public function getType(): int{
        return $this->type;
    }

    public function getText(){
        return $this->text;
    }

    public function getWords(): array{
        return $this->words;
    }

    public function isCommand(): bool{
        return $this->isCommand;
    }

    public function getCommand(bool $stripUsername = false){
        if($stripUsername)
            return preg_replace('/@.+$/', '', $this->command);
        return $this->command;
    }

    public function getAudio(){
        return $this->audio;
    }

    public function getDocument(){
        return $this->document;
    }

    public function getPhoto(){
        return $this->photo;
    }

    public function getSticker(){
        return $this->sticker;
    }

    public function getVideo(){
        return $this->video;
    }

    public function getContact(){
        return $this->contact;
    }

    public function getLocation(){
        return $this->location;
    }

    public function getUserJoined(){
        return $this->joined;
    }

    public function getUserLeft(){
        return $this->left;
    }

    public function getNewTitle(){
        return $this->newTitle;
    }

    public function getNewPhoto(){
        return $this->newPhoto;
    }

    public function getPinnedMessage(){
        return $this->pin;
    }

    public function reply(string $text): Query{
        return new Query('sendMessage', [
            'reply_to_message' => $this->id,
            'chat_id'          => $this->chat->getId(),
            'text'             => $text
        ]);
    }
}
