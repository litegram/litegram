<?php

namespace Litegram\Telegram\Result;

class Video extends Result{

    protected function parse(){
        $json = $this->json;

        if(isset($json->thumb))
            $this->thumb = new PhotoSize($json->thumb);
    }

    public function getFileId(): string{
        return $this->json->file_id;
    }

    public function getWidth(): int{
        return $this->json->width;
    }

    public function getHeight(): int{
        return $this->json->height;
    }

    public function getDuration(): int{
        return $this->json->duration;
    }

    public function getThumb(){
        return $this->thumb;
    }

    /** @return string|null */
    public function getMimeType(){
        return $this->json->mime_type ?? null;
    }

    /** @return int|null */
    public function getFileSize(){
        return $this->json->file_size ?? null;
    }
}