<?php

namespace Litegram\Telegram;

use Litegram\Telegram\Result\File;
use GuzzleHttp\Client;

class Api{
    
    /**
     * URI of Telegram's HTTP API when sending methods
     * 
     * @var string
     */
    const URI_FORMAT = 'https://api.telegram.org/bot%s/';

    /**
     * URI of Telegram's HTTP API when fetching media
     * 
     * @var string
     */
	const FILE_DOWNLOAD_URI = 'https://api.telegram.org/file/bot%s/%s';

    /**
     * @var string
     */
    private $token;

    /** @var Client */
    private $client;

    /** @var Query|void */
    private $lastQuery;

    /**
     * @param string $token Telegram bot token
     */
    public function __construct(string $token){
        $this->token = $token;
        $this->client = new Client([
            'base_uri' => sprintf(self::URI_FORMAT, $token),
            'http_errors' => false,
        ]);
    }

    /**
     * Sends a query to the API and returns unfiltered output.
     * 
     * @param Query $q Query to send
     * 
     * @return string Reply from the API
     */
    public function raw(Query $q): string{
        $this->lastQuery = $q;
        $response = $this->client->post(
            $q->getMethod(),
            $q->getParameters(true)
        );

        return $response->getBody();
    }

    /**
     * - NYI -
     * 
     * Sends a query and returns parsed output in the form of a Result or
     * ResultArray object.
     * 
     * @throws TelegramException
     * @return Result|ResultArray
     */
    public function query(Query $q){
        return Parser::parse($this->raw($q, false), $q->getMethod());
    }

    /**
     * Checks if a given bot token is the same as the one stored in the object.
     * 
     * @param string $in Token to check
     * 
     * @return bool
     */
    public function verifyToken(string $in): bool{
        return ($in == $this->token);
    }

    /**
     * Fetches a file from Telegram's servers.
     * 
     * @param File $file File to fetch.
     * 
     * @return OutFile
     */
    public function dlFile(File $file){        
        $uri = sprintf(self::FILE_DOWNLOAD_URI, $this->token, $file->getFilePath());

        $r = $this->client->get($uri);

        if($r->getStatusCode() != 200)
            throw new TelegramException('File download failed: '.
                $r->getStatusCode().' '.$r->getReasonPhrase());

        return new OutFile($file, $r->getBody());
    }

    /**
     * Shorthand for quickly getting queries done. First argument must be an
     * array containing query parameters. Second argument is an optional bool:
     * true to call `Api::raw()` or false to call `Api::query()`. Defaults to
     * false.
     * 
     * Example:
     * `$api->getUpdates(['offset' => 123], true);`
     * Same thing as:
     * `$api->raw(new Query('getUpdates', ['offset' => 123]))`
     * 
     * 
     */
    public function __call(string $method, array $arguments){        
        $parameters = (array) ($arguments[0] ?? []);
        $raw        = (bool)  ($arguments[1] ?? false);
        $q          = new Query($method, $parameters);

        if($raw)
            return $this->raw($q);
        else
            return $this->query($q);
    }
}