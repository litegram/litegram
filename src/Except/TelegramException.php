<?php

namespace Litegram\Except;

/**
 * Exception caused by something Telegram-related.
 */
class TelegramException extends LitegramException{}