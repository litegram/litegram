<?php

namespace Litegram\Except;

/**
 * Exception caused by the user.
 */
class Id10tException extends LitegramException{}