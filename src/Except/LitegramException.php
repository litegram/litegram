<?php

namespace Litegram\Except;

use Exception;

/**
 * Exception caused by something in Labrys.
 */
class LitegramException extends Exception{}