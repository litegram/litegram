<?php

namespace Litegram\Helpers;

use Litegram\Telegram\Query;

class Markdown{

    public static function link(string $url, string $text): string{
        $text = str_replace(['[', ']'], ['\[', '\]'],   $text);
        $url  = str_replace(['(', ')'], ['%28', '%29'], $url);

        return "[$text]($url)";
    }

    public static function escape(string $text): string{
        return str_replace(
            ['_',  '*',  '`',  '[' ],
            ['\_', '\*', '\`', '\['],
            $text
        );
    }

    public static function bold(string $text): string{
        return "*$text*";
    }

    public static function italics(string $text): string{
        return "_".$text."_";
    }

    public static function pre(string $text): string{
        return "`$text`";
    }

    public static function code(string $text): string{
        return "```\n$text\n```";
    }

    public static function setMode(Query $q): Query{
        return $q->setParameter('parse_mode', 'Markdown');
    }
}