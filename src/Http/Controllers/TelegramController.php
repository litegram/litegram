<?php

namespace Litegram\Http\Controllers;

use Litegram\App;
use Litegram\Telegram\Api;
use Litegram\Telegram\Parser;
use Litegram\Telegram\Query;
use Litegram\Telegram\Router;
use Litegram\Telegram\Result\Update;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface as Container;
use Litegram\Handlers\ErrorHandler;

class TelegramController implements Controller{

    /** @var Router */
    private $r;

    /** @var Api */
    private $api;

    /** @var ErrorHandler */
    private $errorHandler;

    public function __construct(Router $r, Api $api, ErrorHandler $eh){
        $this->r   = $r;
        $this->api = $api;

        $this->errorHandler = $eh;
    }

    public function addRoutes(App $app){
        // Webhook handling
        $app->get('/bot/webhook/{token}', [__CLASS__, 'getWebhook']);
        $app->post('/bot/webhook/{token}', [__CLASS__, 'postWebhook']);

        // Botpic serving
        $app->get('/bot/botpic', [__CLASS__, 'getBotpic']);
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param Container $container
     * @param string   $token
     */
    public function postWebhook($request, $response, Container $c, string $token){
        if(!$this->api->verifyToken($token)){
            return $response->withStatus(403);
        }

        $u = Parser::parseWebhook($request->getBody());
        $m = $u->getMessage();

        if(!$m)
            return $response;

        $c->set('request', $request->withAttribute('tg-message', $m));

        try{
            $q = $this->r->dispatch($m);
        }catch(\Throwable $e){
            return $this->errorHandler->handleTelegram($m, $response, $e);
        }

        if($q instanceof Query){
            if($q->hasFiles()){
                $this->api->raw($q);
                return $response;
            }else
                return $q->getPostResponse($response);
        }
    }

    public function getWebhook($response, $token){
        if(!$this->api->verifyToken($token)){
            return $response->withStatus(403);
        }

        $response->getBody()->write('NYI');
        return $response;
    }

    public function getBotpic(Request $request, Response $response){
        $q = new Query('getUserProfilePhotos');
        $q->setParameter('user_id', $this->api->getMe()->getId());
        $upp = $this->api->query($q);

        $get = $request->getQueryParams();

        $size = $get['size'] ?? 0;

        if(!isset($upp[0][$size])){
            $response->getBody()->write('Unavailable size.');
            return $response->withStatus(404);
        }

        $ps = $upp[0][$size];

        $file = $this->api->getFile(['file_id' => $ps->getFileId()]);
        $f    = $this->api->dlFile($file)->getStream();

        return $response->withHeader('Content-Type', 'image/jpeg')
            ->withBody($f);
    }
}