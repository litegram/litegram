<?php

namespace Litegram\Http\Controllers;

use Litegram\App;

interface Controller{

    public function addRoutes(App $app);
}