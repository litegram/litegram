<?php

namespace Litegram;

use Dotenv\Dotenv;
use Silly\Application;

use Illuminate\Database\Capsule\Manager;

class Kernel{

    // Mutually exclusive; state of ones bit
    const ENV_PROD = 0;
    const ENV_DEV = 1;

    const ENV_CLI = 2;

    /** @var string */
    private $basedir = "";
    
    /** @var int */
    private $env;

    /** @var App */
    private $slim;

    /** @var Application */
    private $silly;

    public function __construct(string $basedir, int $env){
        $this->basedir = realpath($basedir);
        $this->env     = $env;

        $this->parseEnv();

        $this->slim = new App();

        $c = $this->slim->getContainer();
        $c->set('basedir', $this->basedir);
        $c->set('app.environment', $this->env);

        // Summon Eloquent first so all the models get connections.
        $c->get(Manager::class);

        if($env & self::ENV_CLI != 0){
            foreach($c->get('http.controllers') as $cc){
                $cl = $c->get($cc);
                $cl->addRoutes($this->slim);
            }
        }else{
            // Initiate Silly with the same container.
            $this->silly = new Application();
            $this->silly->useContainer($c);
            foreach($c->get('cli.commands') as $cc){
                $cmd = $c->get($cc);
                $cmd->addCommands($this->silly);
            }
        }
    }

    public function parseEnv(){
        $dotenv = new Dotenv($this->basedir);
        $dotenv->load();
    }

    public function getSlim(): App{
        return $this->slim;
    }

    public function getSilly(){
        return $this->silly;
    }

    public function runHttp(){
        $this->slim->run();
    }

    public function runCli(){
        $this->silly->run();
    }
}