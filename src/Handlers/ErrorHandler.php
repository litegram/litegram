<?php

namespace Litegram\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class ErrorHandler extends AbstractError{

    /**
     * Default Litegram PHP error handler. Replaces Slim's error handler.
     * 
     * @param Request   $request
     * @param Response  $response
     * @param Exception $exception
     * 
     * @return Response
     */
    public function __invoke($request, $response, $exception){
        $this->logThrowable($exception);

        $type = $this->determineContentType($request);

        switch($type){
            case 'application/json':
                $out = $this->renderJson($error);
                break;
            case 'text/plain':
                $out = $this->renderPlainText($error);
                break;
            case 'text/html':
            default:
                $type = 'text/html';
                $out = $this->renderHtml($exception);
                break;
        }

        return $response
            ->withStatus(500)
            ->withHeader('Content-Type', $type)
            ->withBody($out);
    }
}