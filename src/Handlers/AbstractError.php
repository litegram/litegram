<?php

namespace Litegram\Handlers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Container\ContainerInterface;
use Litegram\Telegram\Result\Message;
use Slim\Handlers\AbstractHandler;
use Psr\Log\LoggerInterface;
use Throwable;
use Slim\Http\Body;
use Litegram\Telegram\Query;
use Litegram\Helpers\Markdown;

abstract class AbstractError extends AbstractHandler{

    /** @var string[] */
    protected $knownContentTypes = [
        'application/json',
        'text/html',
        'text/plain'
    ];

    /** @var bool */
    protected $showDetails;

    /** @var string */
    protected $guid = '';

    /** @var LoggerInterface */
    protected $logger;

    /** @var callable|null */
    protected $htmlHandler;

    public function __construct(LoggerInterface $logger, ContainerInterface $c){
        $this->logger = $logger;
        $this->showDetails = $c->get('settings')['displayErrorDetails'];
        $this->htmlHandler = $c->get('http.html-error-handler');
        $this->guid = uniqid();
    }

    protected function renderJson(Throwable $t){
        if($this->showDetails){
            $errInfo = [
                'type'    => get_class($t),
                'message' => $t->getMessage(),
                'guid'    => $this->guid
            ];
        }else{
            $errInfo = [
                'message' => 'An error occurred.',
                'guid'    => $this->guid
            ];
        }

        $json = json_encode([
            'ok'    => false,
            'error' => $errInfo
        ]);

        $body = new Body(fopen('php://temp', 'r+'));
        $body->write($json);

        return $body;
    }

    protected function renderPlainText(Throwable $t){
        $out = "An error occurred!\n";
        if($this->showDetails){
            $out.= "Type: ".get_class($t)."\n";
            $out.= "Message: ".$t->getMessage()."\n";
        }
        $out.= "GUID: ".$this->guid;

        $body = new Body(fopen('php://temp', 'r+'));
        $body->write($out);

        return $body;
    }

    protected function renderHtml(Throwable $t){
        $body = new Body(fopen('php://temp', 'r+'));
        $handler = $this->htmlHandler;
        $body->write($handler($t, $this->showDetails));

        return $body;
    }

    /**
     * Error handler for Telegram webhooks.
     * 
     * This handler only supports JSON, returns a 200 OK status and returns a
     * Telegram method. It is not advised to use this outside the context of
     * Telegram.
     * 
     * @param Message  $message
     * @param Response $response
     *
     * @return Response
     */
    public function handleTelegram(Message $message, Response $response, Throwable $t){
        $this->logThrowable($t);

        $text = Markdown::bold('Error')."\n";
        if($this->showDetails){
            $text.= Markdown::bold('Type').': '.Markdown::escape(get_class($t))."\n";
            $text.= Markdown::bold('Message').': '.Markdown::escape($t->getMessage())."\n";
        }
        $text.= Markdown::bold('GUID').': '.Markdown::escape($this->guid);

        $q = Markdown::setMode($message->reply($text));
        $response = $q->getPostResponse($response);

        return $response;
    }

    /**
     * Logs a throwable to the PSR-3 logger.
     * 
     * @param Throwable $t
     * 
     * @return void
     */
    public function logThrowable(Throwable $t){
        $out = 'Uncaught '.get_class($t);
        $this->logger->error($out, ['exception' => $t, 'guid' => $this->guid]);
    }
}