<?php

namespace Litegram\Handlers;

use Throwable;

/**
 * Default HTML renderer for errors.
 */
class HtmlRenderer{

    public function __invoke(Throwable $t, bool $display){
        $html = '<h1>An error occurred</p>';

        return $html;
    }
}