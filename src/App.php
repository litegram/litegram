<?php

namespace Litegram;

use DI\Bridge\Slim\App as DiApp;
use DI\ContainerBuilder;

class App extends DiApp{

    /**
     * @{inheritdoc}
     */
    protected function configureContainer(ContainerBuilder $builder){
        // Add default definitions for Litegram.
        $builder->addDefinitions(__DIR__.'/config.php');
    }
}