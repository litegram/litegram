<?php

namespace Litegram;

use Psr\Container\ContainerInterface;


class Manual{

    /** @var string */
    private $manDir;

    /** @var string[] */
    private $manuals;

    public function __construct(ContainerInterface $c){
        $this->manDir  = $c->get('mandir');
        $this->manuals = scandir($this->manDir);
    }

    public function getPage(string $page){
        $page.= '.md';
        if(!in_array($page, $this->manuals))
            return null;
        
        return file_get_contents($this->manDir.'/'.$page);
    }

    public function getCommandHelp(string $command){
        $page = 'command.'.$command;
        return $this->getPage($page);
    }
}