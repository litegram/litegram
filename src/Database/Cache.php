<?php

namespace Litegram\Database;

use Litegram\Database\Models\Cache as Model;
use DateTime;

class Cache{

    private function isExpired(string $name){
        $cache = Model::where('name', $name);
        if($cache->count() != 1)
            return true;
        $cache = $cache->first();

        $now     = new DateTime('now');
        $expires = new DateTime($cache->expiry);

        if($now->getTimestamp() >= $expires->getTimestamp()){
            $cache->delete();
            return true;
        }else
            return false;
    }

    /**
     * Gets a cached string from the database or null if the cache expired.
     * 
     * @param string $name
     * 
     * @return string|null
     */
    public function getCache(string $name){
        if($this->isExpired($name))
            return null;
        
        return Model::where('name', $name)->first()->value;
    }

    public function setCache(
    string $name, string $value, DateTime $expires=null){
        $cache = new Model();
        $cache->name   = $name;
        $cache->value  = $value;
        $cache->expiry = $expires ?? new DateTime('next week');
        $cache->save();
    }

}