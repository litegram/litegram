<?php

namespace Litegram\Database;

use Litegram\Database\Models\Apikeys;

class KeyManager{

    public function getKey(string $name): string{
        $key = Apikeys::where('name', $name);
        if($key->count() < 1)
            return '';
        return $key->first()->key;
    }
}