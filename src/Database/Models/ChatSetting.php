<?php

namespace Litegram\Database\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Chat-specific settings
 * 
 * - `nsfw` (bool) Disable NSFW output
 * - `disabled` (string) Disabled commands and events
 */
class ChatSetting extends Model{

    protected $table = 'chat-settings';

}