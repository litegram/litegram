<?php

namespace Litegram;

use function DI\create;
use function DI\env;
use function DI\factory;

use Psr\Container\ContainerInterface;
use Litegram\Telegram\Query;
use Slim\Views\TwigExtension;

use Slim\Views\Twig;

use Litegram\Telegram\Modules\Core;

use Litegram\Http\Controllers\TelegramController;
use Litegram\Http\Controllers\HomeController;
use Litegram\Http\Controllers\ErrorController;

use Illuminate\Database\Capsule\Manager as Capsule;

use Litegram\Cli\Commands\Keychain;
use Litegram\Cli\Commands\WebhookEmulator;

use Invoker\ParameterResolver\AssociativeArrayResolver;
use Invoker\ParameterResolver\Container\TypeHintContainerResolver;
use Invoker\ParameterResolver\DefaultValueResolver;
use Invoker\ParameterResolver\ResolverChain;
use Invoker\Invoker;
use Psr\Log\LoggerInterface;
use Monolog\Logger;
use Litegram\Handlers\ErrorHandler;
use Litegram\Handlers\PhpErrorHandler;
use Litegram\Handlers\HtmlRenderer;

return [

    // Slim
    //

    Twig::class => factory(function (ContainerInterface $c){
        $resDir = $c->get('twigdir');
        $view = new Twig($resDir);

        $basePath = rtrim(str_ireplace('index.php', '', $c->get('request')
            ->getUri()->getBasePath()), '/');
        
        $view->addExtension(new TwigExtension($c->get('router'), $basePath));
        return $view;
    }),

    'view' => \DI\get(Twig::class),

    // Monolog
    //

    'monolog.name'       => 'Litegram',
    'monolog.handlers'   => [],
    'monolog.processors' => [],

    'errorHandler'    => \DI\get(ErrorHandler::class),
    'phpErrorHandler' => \DI\get(PhpErrorHandler::class),

    'http.html-error-handler' => \DI\get(HtmlRenderer::class),

    LoggerInterface::class => function (ContainerInterface $c) {
        $name   = $c->get('monolog.name');
        $logger = new Logger($name);

        foreach($c->get('monolog.handlers') as $handler)
            $logger->pushHandler($handler);

        foreach($c->get('monolog.processors') as $processor)
            $logger->pushProcessor($processor);

        return $logger;
    },

    'logger' => \DI\get(LoggerInterface::class),

    // Eloquent
    //

    Capsule::class => function (ContainerInterface $c){
        $capsule = new Capsule;
        $capsule->addConnection($c->get('db'));

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return $capsule;
    },

    // Telegram
    //

    Telegram\Api::class => create()->constructor(\DI\get('telegram.token')),

    'telegram.modules' => [
        Core::class
    ],

    ModuleInvoker::class => function (ContainerInterface $c){
        return new ModuleInvoker($c->get('telegram.invoker'));
    },

    'telegram.invoker' => function (ContainerInterface $c) {
        $resolvers = [
            new AssociativeArrayResolver(),
            new TypeHintContainerResolver($c),
            new DefaultValueResolver()
        ];

        return new Invoker(new ResolverChain($resolvers));
    },

    // Http
    //

    'http.controllers' => [
        TelegramController::class
    ],

    // Command line
    //

    'cli.commands' => [
        WebhookEmulator::class,
        Keychain::class
    ],

    // Directory structure

    'resdir' => \DI\string('{basedir}/res'),
    'mandir' => \DI\string('{resdir}/man'),
    'twigdir' => \DI\string('{resdir}/twig')

];
