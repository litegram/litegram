# Litegram

Slim-based framework for Telegram bots.

**Note**: This repository contains only the core functions of Litegram. If you
want to make an application using Litegram, please use
[litegram/skeleton](https://gitlab.com/litegram/skeleton).